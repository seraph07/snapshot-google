var express = require('express');
var app = express();

app.get('/', function(req,res){
    var codigoCorreo = generarCorreo(req.query.id);

	res.send('<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">'
		+ '<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">'
		+ '<head> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> <meta name="viewport" content="width=device-width, initial-scale=1">'
		+ '<meta http-equiv="X-UA-Compatible" content="IE=edge"> <meta name="format-detection" content="date=no"> <meta name="format-detection" content="telephone=no">'
		+ '<title>Single Column</title> <style type="text/css">body{margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;}'
		+ 'table{border-spacing: 0;}table td{border-collapse: collapse;}.ExternalClass{width: 100%;}.ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div{line-height: 100%;}'
		+ '.ReadMsgBody{width: 100%; background-color: #ffd110;}table{mso-table-lspace: 0pt; mso-table-rspace: 0pt;}img{-ms-interpolation-mode: bicubic;}.yshortcuts a{border-bottom: none !important;} '
		+ '@media screen and (max-width: 599px){.force-row, .container{width: 100% !important; max-width: 100% !important;}}@media screen and (max-width: 400px){.container-padding{padding-left: 12px !important; padding-right: 12px !important;}}'
		+ '.ios-footer a{color: #aaaaaa !important; text-decoration: underline;}a[href^="x-apple-data-detectors:"],a[x-apple-data-detectors]{color: inherit !important; text-decoration: none !important; font-size: inherit !important; font-family: inherit !important; '
		+ 'font-weight: inherit !important; line-height: inherit !important;}</style></head>'
		+ '<body style="margin:0; padding:0;" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">'
		+ '<table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF"> <tr> '
		+ '<td align="center" valign="top" bgcolor="#FFFFFF" style="background-color: #FFFFFF;"> '
		+ '<br><table border="0" width="600" cellpadding="0" cellspacing="0" class="container" style="width:600px;max-width:600px"> <tr> '
		+ '<td class="container-padding header" align="left" style="font-family:Helvetica, Arial, sans-serif;font-size:24px;font-weight:bold;padding-bottom:12px;color:#000000;padding-left:24px;padding-right:24px"> '
		+ '<div><img src="http://www.daduga.com.mx/_/rsrc/1447164808653/config/customLogo.gif" style="width: 70px; height: 70px;"></div>Grupo DADUGA </td></tr><tr> '
		+ '<td class="container-padding content" align="left" style="padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff"> <br>'
		+ codigoCorreo
		+ '<br><div class="body-text" style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333"> '
		+ 'Para más información, consulte al Encargado en sistemas sobre la creación de respaldos automáticos de los discos virtuales en la nube de GCP <br>'
		+ '<br></div></td></tr><tr style="background-color: #7E4C4D">'
		+ '<td><br></td></tr><tr style="background-color: #ECD390"><td><br></td></tr><tr style="background-color: #EA6B5C"><td>'
		+ '<br></td></tr><tr> <td class="container-padding footer-text" align="left" style="font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#FFFFFF;padding-left:24px;padding-right:24px;background-color:#3B446B"> '
		+ '<br><br>Grupo DADUGA <br><br>Este correo electrónico ha sido enviado por la Función de respaldos en GCP <br><br>'
		+ ''
		+ '<span class="ios-footer"> Poniente 116 No. 690<br>Industrial Vallejo, CDMX, 02300<br></span>'
		+ ' <a href="http://www.daduga.com.mx" style="color:#aaaaaa">www.daduga.com.mx</a><br><br><br></td></tr><tr></tr></table> '
		+ '</td></tr></table></body></html>');
});



app.listen(3000);


function generarCorreo(codigoSalida){
	var codigoCorreo = '';
	switch(codigoSalida){
		case '1':
			codigoCorreo = '<div class="title" style="font-family:Helvetica, Arial, sans-serif;font-size:18px;font-weight:600;color:#5cb85c">!El respaldo se creo correctamente¡</div>';
		break;
		case '2':
			codigoCorreo = '<div class="title" style="font-family:Helvetica, Arial, sans-serif;font-size:18px;font-weight:600;color:#f9870b">!Necesitamos su atención¡</div>';
		break;
		case '3':
			codigoCorreo = '<div class="title" style="font-family:Helvetica, Arial, sans-serif;font-size:18px;font-weight:600;color:#d9534f">!Algo ha salido mal¡</div>';
		break;
		default:
			codigoCorreo = '<div class="title" style="font-family:Helvetica, Arial, sans-serif;font-size:18px;font-weight:600;color:#a8a8a8">!No sabemos que ha pasado¡</div>';

	};
	return codigoCorreo;
}