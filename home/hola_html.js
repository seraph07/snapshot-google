var http = require("http"),
	fs = require("fs");

var html = fs.readFileSync("./progsin.html");
//funcion para obtener la fecha en una cadena simple
var dateToday = new Date();

if (dateToday.getDay() < 10){
	var dateSingleToday = (dateToday.getDay()).toString();
	var dayToday = "0"+dateSingleToday	
} else {
	var dayToday = (dateToday.getDay()).toString();
}

var monthToday = (dateToday.getMonth()).toString();
var yearToday = (dateToday.getFullYear()).toString();

var hourToday = (dateToday.getHours()).toString();
var minutesToday = (dateToday.getMinutes()).toString();

var stringDate = dayToday+monthToday+yearToday+hourToday+minutesToday;

//funcion nombrar snapshot

var nameSnapshot = "snapshot" + stringDate;
console.log(nameSnapshot);

http.createServer(function(req,res){
	res.write(html);
	res.end();
}).listen(8080);