var express = require('express');
var app = express();


app.get('/', function(req,res){
	
	var nombre = req.query.nombre || '';
	var apellido = req.query.apellido || '';
	var saludo = '';

	if (nombre!='')
		saludo = "Hola " + nombre + " " + apellido;

	res.send('<html><body>'
					+ '<h1>Saludo</h1>'
					+ '<p>' + saludo + '</p>'
					+ '<form method="get" action="/">'
					+ '<label for="nombre">¿Cómo te llamas?</label>'
					+ '<input type="text" name="nombre" id="nombre">'
					+ '<input type="text" name ="apellido" id ="apellido">'
					+ '<input type="submit" value="Enviar"/>'
					+ '</form>'
					+ '</body></html>');
});

app.listen(3000);