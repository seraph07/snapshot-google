/**
*Script para ejecutar un snapshot
Este script consta de varios verificadores de seguridad.
Se necesita un Key unico para cada proyecto generado por el usuario.
Solo se aceptan los parametros por GET, otro metodo es descartado
Los parametros se Parsean con el Path /param

los parametros son:
nameproyect /es el nombre del proyecto en google cloud
zoneproyect /la zona en donde el usuario le indico a GCP donde almacene el proyecto
diskproyect /el nombre del disco, de la instancia a la que se le hara el snapshot
guestflushactive /valor boobleano parseado como string, para activar el VSS de Windows Server (recomendado en True)
namebeforedate /nombre que se asigna, anterior a un identificador que es la fecha y hora en el que se crea el snapshot, preferente el nombre del proyecto
**/

/*Funcion  principal de GCP Functions, evaluacion del path*/
exports.snapshot = function snapshot(req,res){
	const path = req.path //evaluacion del path
	switch(path){
		case '/param':
		executeSnapshot(req,res); // se llama a la función executeSnapshot para que se ejecute en esta función principál
		break;
	default:
		res.status(200).send("El servidor sigue escuchando"); //en caso de que el URL no traiga el path /param, no hara llamado a ninguna función.
	}
}; //fin de la ejecución  de la función principal.

/*Función para ejecutar el snapshot*/
const executeSnapshot = (req,res) => {
	if (req.method === 'GET') { //Se verifica que sea por el metodo GET solamente el paso de parametros
		var miKey = 'YC6n8ZHYK2ZpvKCs47n9';
		var theKey = req.query.key;
		if (theKey === miKey) { //Verificacion de la KEY unica, establecida por el usuario
			var nameProyect = req.query.nameproyect;//nombre del proyecto
			var zoneProyect = req.query.zoneproyect;//zona del proyecto
			var diskProyect = req.query.diskproyect;//disco del proyecto
			var guestFlushActive = req.query.guestflushactive; //activiacion del VSS
			var nameBeforeDate = req.query.namebeforedate;//nombre identificador dado al proyecto
			nameBeforeDate = nameBeforeDate.toLowerCase();
			/*Funcion para crear el nombre*/
			//generador de la fecha
			var dateToday = new Date();

			function noZeroInTheDate(digit){
				var myDigit;
				if (digit<10) {
					myDigit = "0" + digit.toString();
				} else {
					myDigit = digit.toString();
				}
				return myDigit;
			}

			var dayToday = noZeroInTheDate(dateToday.getDate());
			var monthToday = noZeroInTheDate(dateToday.getMonth()+1);
			var yearToday = noZeroInTheDate(dateToday.getFullYear());
			var hourToday = noZeroInTheDate(dateToday.getUTCHours()-6);
			var minutesToday = noZeroInTheDate(dateToday.getMinutes());

			//generador del nombre que se le asignara al snapshot
			var nameSnapshot = nameBeforeDate + dayToday + monthToday + yearToday + hourToday + minutesToday;

			/*Funcion para crear el Spashot*/
			//importación de librerias requeridas
			var google = require('googleapis');
			var compute = google.compute('beta');


			//llamado a la función con los parametros obtendios del URL
			authorize(function(authClient){
				var request = {
					project: nameProyect,
					zone: zoneProyect,
					disk: diskProyect,
					guestFlush: guestFlushActive,
					resource: {
						name: nameSnapshot,
					},
					auth: authClient,
				};

				//Ejecunción para crear el snapshot
				compute.disks.createSnapshot(request, function(err, response){
					if (err){
						console.error(err);
						return;
					}
					console.log(JSON.stringify(response, null, 2));
				});
			});

			//función que valida la autentificación de las crecenciales de google
			//usar Google Auth 2.0
			function authorize(callback) {
	      		google.auth.getApplicationDefault(function(err, authClient) {
	        		if (err) {
	          			console.error('authentication failed: ', err);
	          			return;
	        		}
	        		if (authClient.createScopedRequired && authClient.createScopedRequired()) {
	          			var scopes = ['https://www.googleapis.com/auth/cloud-platform'];
	          			authClient = authClient.createScoped(scopes);
	        		}
	        		callback(authClient);
	      		});
	      	}

	      	sendEmail();
			res.status(200).send('Trabajo completado con exito.');
		}//fin del if que verifico la paridad con los keys
	} else { //fin del if que valido si se recibio la solicitud por GET, en caso de que sea por otro metodo mandara error.
		res.status(404);
	}
}//fin de execute snapshot.

/*Funciòn para enviar un enviar un correo de notificación*/
const sendEmail = () => {
	const sgMail = require('@sendgrid/mail');  //llamar a las librerias de SendGrid en Packajes.JSON
	sgMail.setApiKey('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');
	const msg = { //cuerpo del correo
		to: 'sistemas@daduga.com.mx',
		from: 'sistemas@daduga.com.mx',
		subject: 'Aviso creación de respaldos en el servidor de ADVAN PRO',
		text: 'Se ha creado el respaldo programado de ADVAN PRO y sus datos almacenados en la instacia virutal de GCP.',
		html: '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office"><head> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> <meta name="viewport" content="width=device-width, initial-scale=1"> <meta http-equiv="X-UA-Compatible" content="IE=edge"> <meta name="format-detection" content="date=no"> <meta name="format-detection" content="telephone=no"> <title>Single Column</title> <style type="text/css">body{margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;}table{border-spacing: 0;}table td{border-collapse: collapse;}.ExternalClass{width: 100%;}.ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div{line-height: 100%;}.ReadMsgBody{width: 100%; background-color: #ffd110;}table{mso-table-lspace: 0pt; mso-table-rspace: 0pt;}img{-ms-interpolation-mode: bicubic;}.yshortcuts a{border-bottom: none !important;}@media screen and (max-width: 599px){.force-row, .container{width: 100% !important; max-width: 100% !important;}}@media screen and (max-width: 400px){.container-padding{padding-left: 12px !important; padding-right: 12px !important;}}.ios-footer a{color: #aaaaaa !important; text-decoration: underline;}a[href^="x-apple-data-detectors:"],a[x-apple-data-detectors]{color: inherit !important; text-decoration: none !important; font-size: inherit !important; font-family: inherit !important; font-weight: inherit !important; line-height: inherit !important;}</style></head><body style="margin:0; padding:0;" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"><table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF"> <tr> <td align="center" valign="top" bgcolor="#FFFFFF" style="background-color: #FFFFFF;"> <br><table border="0" width="600" cellpadding="0" cellspacing="0" class="container" style="width:600px;max-width:600px"> <tr> <td class="container-padding header" align="left" style="font-family:Helvetica, Arial, sans-serif;font-size:24px;font-weight:bold;padding-bottom:12px;color:#000000;padding-left:24px;padding-right:24px"> <div><img src="http://www.daduga.com.mx/_/rsrc/1447164808653/config/customLogo.gif" style="width: 70px; height: 70px;"></div>Grupo DADUGA </td></tr><tr> <td class="container-padding content" align="left" style="padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff"> <div class="title" style="font-family:Helvetica, Arial, sans-serif;font-size:18px;font-weight:600;color:#5cb85c">!El respaldo se creo correctamente¡</div><br><div class="body-text" style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333"> Se ha creado una captura del disco, de la instancia en donde se encuentra ADVAN PRO en la platadorma de Google Cloud. <br><br>Para mas información consulte con el encargado de sistemas. <br><br></div></td></tr><tr style="background-color: #7E4C4D"><td style="height:10px;"></td></tr><tr style="background-color: #ECD390"><td style="height:10px;"></td></tr><tr style="background-color: #EA6B5C"><td style="height:10px;"></td></tr><tr> <td class="container-padding footer-text" align="left" style="font-family:Helvetica, Arial, sans-serif;font-size:11px;line-height:16px;color:#FFFFFF;padding-left:24px;padding-right:24px;background-color:#3B446B"> <br>Este correo electrónico ha sido enviado por la Función de respaldos en GCP <br><br><table style="text-align: center;"> <tr> <td><strong>Auto Tanques Estrella S. de R.L. de C.V.</strong></td><td><strong>Auto Liqui Refinados S.A. de C.V.</strong></td><td><strong>Transportes David Duran García S.A. de C.V.</strong></td></tr></table> <br><span class="ios-footer"> Poniente 116 No. 690<br>Industrial Vallejo, CDMX, 02300<br></span> <a href="http://www.daduga.com.mx" style="color:#aaaaaa">www.daduga.com.mx</a><br><br></td></tr><tr></tr></table> </td></tr></table></body></html>',
	};//fin del JSON con los parametros que contendra el correo electronico
	sgMail.send(msg);//enviar el correo
}//fin de la función de envio de correo